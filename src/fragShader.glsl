precision mediump float;
varying vec3 fragColor;
varying vec3 vertPosition;
void main() {
  vec3 color = fragColor;
  gl_FragColor = vec4(color, 1);
}