import './App.css';
import WebGLCanvas from './WebGLCanvas';
import { initDemo } from './graphics';

function App() {
  return (
    <div className="App">
      <WebGLCanvas
        initDemo={initDemo}
      />
    </div>
  );
}

export default App;
