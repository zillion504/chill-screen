import fragmentShaderText from "./fragShader.glsl"
import vertexShaderText from "./vertShader.glsl"
import { mat4 } from "gl-matrix"

const lerp = (x, y, t) => x + (y-x) * t

const lerpColor = (color1, color2, t) => {
  const [r1,g1,b1] = color1
  const [r2,g2,b2] = color2

  return [
    lerp(r1, r2, t),
    lerp(g1, g2, t),
    lerp(b1, b2, t),
  ]
}

const generateMap = () => {
  var vertexPositions = []
  var vertexColors = []
  var vertexIndices = []

  const rows = 30
  const cols = 30

  const lf = [ 86/255, 40/255, 210/255 ]
  const rf = [ 150/255, 40/255, 100/255 ]
  const lb = [ 43/255, 0/255, 78/255 ]
  const rb = [ 43/255, 0/255, 78/255 ]

  for (let i = 0; i < rows; i++) {
    const it = i / rows
    const rowColorFront = lerpColor(lf, rf, it)
    const rowColorBack = lerpColor(lb, rb, it)

    for (let j = 0; j < cols; j++) {
      const jt = j / cols
      vertexPositions.push(
        lerp(-5, 5, it),
        lerp(-1, 0, jt),
        lerp(-5, 5, jt)
      )

      const color = lerpColor(rowColorBack, rowColorFront, jt)
      vertexColors.push(...color)
    }
  }

  const v = (i, j) => j + i * rows

  for (let i = 0; i < rows-1; i++) {
    for (let j = 0; j < cols-1; j++) {
      /*
        v3-v4
        |   |
        v1-v2
      */
      const v1 = v(i, j)
      const v2 = v(i + 1, j)
      const v3 = v(i, j + 1)
      const v4 = v(i + 1, j + 1)

      vertexIndices.push(
        v1,
        v2,
        v3
      )

      vertexIndices.push(
        v4,
        v3,
        v2
      )
    }
  }

  return {
    vertexPositions,
    vertexColors,
    vertexIndices
  }
}

const read = (url) => fetch(url).then(res => res.text())

const initDemo = async (canvas, id) => {
  let gl = canvas.getContext("webgl")

  if (!gl) {
    gl = canvas.getContext("experimental-webgl")
  }

  if (!gl) {
    alert("Your browser does not support WebGL rendering")
    return
  }

  gl.enable(gl.DEPTH_TEST)
  // gl.enable(gl.CULL_FACE)
  gl.clearColor(43/255, 0, 78/255, 1.0)

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

  // Create shaders
  const vertexShader = gl.createShader(gl.VERTEX_SHADER)
  const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER)

  // Compile shaders
  gl.shaderSource(vertexShader, await read(vertexShaderText))
  gl.shaderSource(fragmentShader, await read(fragmentShaderText))

  gl.compileShader(vertexShader)
  if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
    console.error("Error compiling vertex shader!", gl.getShaderInfoLog(vertexShader))
    return
  }

  gl.compileShader(fragmentShader)
  if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
    console.error("Error compiling fragment shader!", gl.getShaderInfoLog(fragmentShader))
    return
  }

  const program = gl.createProgram()
  gl.attachShader(program, vertexShader)
  gl.attachShader(program, fragmentShader)

  gl.linkProgram(program)

  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
    console.error("Error linking program!", gl.getProgramInfoLog(program))
    return
  }

  gl.validateProgram(program)
  if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
    console.error("Error validating program!", gl.getProgramInfoLog(program))
    return
  }

  // Create buffer
  const {
    vertexPositions,
    vertexColors,
    vertexIndices
  } = generateMap()
  
  const vertexPositionBufferObject = gl.createBuffer()
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexPositionBufferObject)
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexPositions), gl.STATIC_DRAW)

  const vertexColorBufferObject = gl.createBuffer()
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexColorBufferObject)
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexColors), gl.STATIC_DRAW)

  var vertexIndexBufferObject = gl.createBuffer()
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vertexIndexBufferObject)
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW)

  const positionAttribLocation = gl.getAttribLocation(program, "vertPosition")
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexPositionBufferObject)
  gl.enableVertexAttribArray(positionAttribLocation)
  gl.vertexAttribPointer(
    positionAttribLocation,
    3,
    gl.FLOAT,
    false,
    3 * Float32Array.BYTES_PER_ELEMENT,
    0,
  )

  const colorAttribLocation = gl.getAttribLocation(program, "vertColor")
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexColorBufferObject)
  gl.enableVertexAttribArray(colorAttribLocation)
  gl.vertexAttribPointer(
    colorAttribLocation,
    3,
    gl.FLOAT,
    false,
    3 * Float32Array.BYTES_PER_ELEMENT,
    0,
  )

  gl.useProgram(program)

  var matWorldUniformLocation = gl.getUniformLocation(program, "mWorld")
  var matViewUniformLocation = gl.getUniformLocation(program, "mView")
  var matProjUniformLocation = gl.getUniformLocation(program, "mProj")

  var x = gl.getUniformLocation(program, "x")

  var viewMatrix = new Float32Array(16);
  var worldMatrix = new Float32Array(16);
  var projMatrix = new Float32Array(16);

  mat4.identity(worldMatrix)
  mat4.lookAt(viewMatrix, [0, 1.5, 5], [0, 0, 0], [0, 1, 0])
  mat4.perspective(projMatrix, 3.14159 * 30/180, 16 / 9, 0.1, 20)

  gl.uniformMatrix4fv(matWorldUniformLocation, false, worldMatrix)
  gl.uniformMatrix4fv(matViewUniformLocation, false, viewMatrix)
  gl.uniformMatrix4fv(matProjUniformLocation, false, projMatrix)
  gl.uniform1f(x, 0)

  // Main render loop
  const identityMatrix = new Float32Array(16)

  let secs = 0
  mat4.identity(identityMatrix)
  const loop = () => {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    secs = performance.now() / 1000

    gl.uniform1f(x, secs)
    mat4.rotate(worldMatrix, identityMatrix, Math.PI * 0, [1, 0, 0])
    gl.uniformMatrix4fv(matWorldUniformLocation, false, worldMatrix)

    gl.drawElements(gl.TRIANGLES, vertexIndices.length, gl.UNSIGNED_SHORT, 0)

    if (canvas.id === id)
      requestAnimationFrame(loop)
  }
  requestAnimationFrame(loop)

}

export {
  initDemo
}
