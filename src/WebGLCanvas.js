import React, { useEffect } from "react"
import { v4 } from "uuid"

const WebGLCanvas = (props) => {
  const { initDemo } = props

  const canvas = React.createRef()

  useEffect(() => {
    const id = v4()
    canvas.current.id = id
    initDemo(canvas.current, id)
  }, [canvas, initDemo])
  
  return (
    <canvas
      width="500px"
      height="500px"
      ref={canvas}
    />
  )
}
export default WebGLCanvas