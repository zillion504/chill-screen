precision mediump float;
attribute vec3 vertPosition;
attribute vec3 vertColor;
varying vec3 fragColor;
uniform mat4 mWorld;
uniform mat4 mView;
uniform mat4 mProj;
uniform float x;

float wave(float wavelength, float amplitude, float x) {
  return sin(x / wavelength * 3.14159 / 2.0) * amplitude;
}

void main() {
  fragColor = vertColor;
  vec3 pos = vertPosition + vec3(
    0, 
    // wave(10.0, 0.2, x) +
    wave(2.0, 0.2, vertPosition.x + vertPosition.z + x) +
    wave(0.4, 0.3, vertPosition.z / 5.0 + x / 10.0) +
    wave(0.8, 0.1, vertPosition.x + x / 20.0),
    0
  );
  gl_Position = mProj * mView * mWorld * vec4(pos, 1.0);
}